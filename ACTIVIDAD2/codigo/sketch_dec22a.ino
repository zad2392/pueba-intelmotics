#include <ESP8266HTTPClient.h> // head of program

const char *host = "https://api.helo.v1.intelmotics.com/services/prueba_tecnica/hola.php";

// Below in the Loop
HTTPClient http; //Declare object of class HTTPClient

//Post Data
String postData = "id=1212&token=1010101010" //"{'id': '1212', 'token':'1010101010'}"
                

                  http.begin(host);                                  //Specify request destination
delay(1000);                                                         // See if this prevents the problm with connection refused and deep sleep
http.addHeader("Content-Type", "application/x-www-form-urlencoded"); //Specify content-type header

int httpCode = http.POST(postData); //Send the request
String payload = http.getString();  //Get the response payload

Serial.println(httpCode); //Print HTTP return code
Serial.println(payload);  //Print request response payload

http.end(); //Close connection